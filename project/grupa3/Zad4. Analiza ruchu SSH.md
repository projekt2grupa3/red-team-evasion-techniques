Uruchamiamy Wireshark

Tworzymy polaczenie SSH uytkownik: uranus, haslo: butterfly 
[Zad4._Analiza_ruch_SSH1.png](project/grupa3/images/Zad4._Analiza_ruch_SSH1.png)

Lugojemy sie na wirtualna maszyne SDA i tworzymy pliki secret1.txt i secret2.txt 
W obu plikach wpisujemy tajne haslo.
[Zad4._Analiza_ruchu_SSH8.png](images/Zad4._Analiza_ruchu_SSH8.png)

Konfigurujemy vsftpd.  
[Zad4._Analiza_ruchu_SSH2.png](images/Zad4._Analiza_ruchu_SSH2.png)
[Zad4._Analiza_ruchu_SSH3.png](images/Zad4._Analiza_ruchu_SSH3.png)


Tworzymy polaczenie FTP uzytkownik: uranus, haslo: butterfly
[Zad4._Analiza_ruchu_SSH4.png](images/Zad4._Analiza_ruchu_SSH4.png)
[Zad4._Analiza_ruchu_SSH5.png](images/Zad4._Analiza_ruchu_SSH5.png)


Znajdujemy i kopiujemy pliki secret1.txt i secret2.txt
[Zad4._Analiza_ruchu_SSH6.png](images/Zad4._Analiza_ruchu_SSH6.png)

Zamykamy polaczenie FTP i SSH.

Analizujemy ruch w Wireshark. Filtrujemy FTP-DATA
[Zad4._Analiza_ruchu_SSH7.png](images/Zad4._Analiza_ruchu_SSH7.png)

[projekt2zad4.pcapng](images/projekt2zad4.pcapng)
