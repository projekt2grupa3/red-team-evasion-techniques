**Zadanie 3 - Analiza ruchu HTTP**
Środowisko: Kali Linux
1. Rozpocznij monitorowanie ruchu sieciowego (narzędziem Wireshark).

Uruchomienie Wireshark

2. W przeglądarce nawiąż połączenie z http://testphp.vulnweb.com/login.php
3. Wykonaj próbę logowania (dowolne dane).

Zrzut ekranu ze złapanymi pakietami: [zad3_analiza_1.png](images/zad3_analiza_1.png)

4. Odszukaj w zapisanym ruchu swoje dane logowania.
Tworzymy filtr `http.request.method == POST`

Wynik: [zad3_analiza_2.png](images/zad3_analiza_2.png)

Dla porownania powtorz ćwiczenie z logowaniem np. do Facebooka (również dowolne,nieprawdziwe dane logowania). 
Dla ruchu HTTPS nie jest mozliwe podgladanie ruchu

[caly ruch](zad3_analiza_fb_1.png)

[filtr http.request.method == POST](zad3_analiza_fb_2.png)
