projekt2:
Do dekodowania hash'y zostal uzyty program hash-identifier:
Zadanie 1
1/3
dekodowanie hash'y:
1. MD5
2. SHA-1
3. sha-512
4. sha-512
Po dekodowaniu 
1. hashcat -a3 -m0 81dc9bdb52d04dc20036dbd8313ed055 -> 1234
2. hashcat -a3 -m100 d8826bbd80b4233b7522d1c538aeaf66c64e259a -> 4121
3. hashcat -a3 -m1700 b021d0862bc76b0995927902ec697d97b5080341a53cd90b780f50fd5886f4160bbb9d4a573b76c23004c9b3a44ac95cfde45399e3357d1f651b556dfbd0d58f -1?d ?1?1?1?1 -> 6969
4. hashcat -a3 -m1700 b021d0862bc76b0995927902ec697d97b5080341a53cd90b780f50fd5886f4160bbb9d4a573b76c23004c9b3a44ac95cfde45399e3357d1f651b556dfbd0d58f -1?d ?1?1?1?1 -> 0

2/3:
dekodowanie hash'y:
1. sha512
2. sha512
Po dekodowaniu 
1. hashcat -a3 -m1700 9e66d646cfb6c84d06a42ee1975ffaae90352bd016da18f51721e2042d9067dcb120accc574105b43139b6c9c887dda8202eff20cc4b98bad7b3be1e471b3aa5 -> sda
2. hashcat -a3 -m1700 8a04bd2d079ee38f1af784317c4e2442625518780ccff3213feb2e207d2be42ca0760fd8476184a004b71bcb5841db5cd0a546b9b8870f1cafee57991077c4a9 -> Asia

3/3:
hashcat -a3 -m1700 44d9886c0a57ddbfdb31aa936bd498bf2ab70f741ee47047851e768db953fc4e43f92be953e205a3d1b3ab752ed90379444b651b582b0bc209a739a624e109da -1 ?l?u?d?s ?1?1?1?1?1?1
SHA-512
44d9886c0a57ddbfdb31aa936bd498bf2ab70f741ee47047851e768db953fc4e43f92be953e205a3d1b3ab752ed90379444b651b582b0bc209a739a624e109da:T0^^3k

Zadanie 2
1/2:
hashcat -a0 -m0 -o cracked.txt hashes_md5.txt /usr/share/wordlists/rockyou.txt
MD-5
9fd8301ac24fb88e65d9d7cd1dd1b1ec:butterfly
6104df369888589d6dbea304b59a32d4:blink182                 
276f8db0b86edaa7fc805516c852c889:baseball                 
7f9a6871b86f40c330132c4fc42cda59:tinkerbell               
04dac8afe0ca501587bad66f6b5ce5ad:hellokitty

2/2:
hashcat -a0 -m1700 -o cracked.txt hashes_sha.txt /usr/share/wordlists/rockyou.txt
SHA-512
7ab6888935567386376037e042524d27fc8a24ef87b1944449f6a0179991dbdbc481e98db4e70f6df0e04d1a69d8e7101d881379cf1966c992100389da7f3e9a:spiderman
470c62e301c771f12d91a242efbd41c5e467cba7419c664f784dbc8a20820abaf6ed43e09b0cda994824f14425db3e6d525a7aafa5d093a6a5f6bf7e3ec25dfa:rockstar






