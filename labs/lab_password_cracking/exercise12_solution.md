Środowisko:

- Kali Linux
- Metasploitable 2 VM

1. [Kali Linux] uzywajac polecenie unshadow "połącz" plik passwd z shadow => unshadow passwd shadow > passwd_and_shadow.txt
3. [Kali Linux] uruchom polecenie john aby zlamac hasla => john passwd_and_shadow.txt --format=md5crypt lub ze słownikiem john passwd_and_shadow.txt --wordlist=/usr/share/wordlists/rockyou.txt --format=md5crypt
4. [Kali Linux] Jakie jest hasło dla konta "sys"?

Ścieżka do pliku wynikowego: /root/.john/john.pot